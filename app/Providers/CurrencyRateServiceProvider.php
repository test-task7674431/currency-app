<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Modules\Shared\CurrencyRate\Services\CurrencyRateService;
use App\Modules\Shared\CurrencyRate\Services\CurrencyRateServiceInterface;

class CurrencyRateServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(CurrencyRateServiceInterface::class, CurrencyRateService::class);
    }
}
