<?php

namespace App\Modules\Shared\CurrencyRate\Services;

use App\Modules\Shared\CurrencyRate\Enums\CurrencyEnum;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class CurrencyRateService implements CurrencyRateServiceInterface
{
    private const API_DAILY_URL = 'https://www.nbkr.kg/XML/daily.xml';
    private const API_WEEKLY_URL = 'https://www.nbkr.kg/XML/weekly.xml';

    public function syncToDb(): void
    {
        $dailyData = $this->fetchRatesFromApi(self::API_DAILY_URL);
        $weeklyData = $this->fetchRatesFromApi(self::API_WEEKLY_URL);
        $mergedData = $this->mergeData($dailyData, $weeklyData);

        // Save the merged data to the database
        foreach ($mergedData as $currency => $rate) {
            DB::table('currency_rates')->updateOrInsert(
                ['currency' => $currency, 'date' => date('Y-m-d')],
                ['rate' => $rate]
            );
        }
    }

    private function fetchRatesFromApi(string $url): array
    {
        $response = Http::get($url);
        $xml = simplexml_load_string($response->body());

        $data = [];
        foreach ($xml as $currency) {
            $currencyCode = (string) $currency['ISOCode']; // Accessing the ISOCode attribute

            // Skip the currency if the CharCode starts with "KGS"
            if (strpos($currencyCode, "KGS") === 0) {
                continue;
            }

            $denomination = (int) $currency->Nominal;
            $rate = (float) str_replace(',', '.', (string) $currency->Value);
            $data[$currencyCode] = ($denomination > 1) ? ($rate / $denomination) : $rate;
        }

        // Manually add KGS with a fixed rate of 1.0
        $data[CurrencyEnum::KGS->name] = 1.0;

        return $data;
    }

    private function mergeData(array $dailyData, array $weeklyData): array
    {
        // Prioritize daily data over weekly data
        return array_merge($weeklyData, $dailyData);
    }

    public function getCurrentRate(CurrencyEnum $currency, $defaultCurrency = CurrencyEnum::KGS): float|string
    {
        try {
              // Handle local currency with a fixed rate of 1
            if ($currency->value === $defaultCurrency) {
                return 1.0;
            }

            $data = $this->fetchRatesFromApi(self::API_DAILY_URL);

            // If default currency is not in the fetched data, manually set its rate to 1
            if (!isset($data[$defaultCurrency->value])) {
                $data[$defaultCurrency->value] = 1.0;
            }

            $currencyCode = $currency->value;
            
            if(!isset($data[$currencyCode])){
                throw new \RuntimeException("{$currencyCode} => Rate not found");
            }

            return $data[$currencyCode];
        } catch (\RuntimeException $e) {
            throw new \RuntimeException("{$currencyCode} => Rate not found");
        }
    }

}
